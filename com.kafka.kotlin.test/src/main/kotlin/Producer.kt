import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.StringSerializer
import org.slf4j.LoggerFactory
import java.util.*

object Producer {
    private val logger = LoggerFactory.getLogger(this::class.java.name)
    private val producerProperties: Properties by lazy {
        val stringSerializer = StringSerializer::class.java.name
        return@lazy Properties().apply {
            setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, ConfigConstants.BOOTSTRAP_SERVERS)
            setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, stringSerializer)
            setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, stringSerializer)
        }
    }
    private val producer: KafkaProducer<String, String>
        get() {
            return KafkaProducer(
                producerProperties
            )
        }

    fun sendMessage(
        message: String = "hello world",
        topic: String = ConfigConstants.DEFAULT_TOPIC,
        key: String? = null
    ) {
        val record = generateRecord(message, topic, key)


        
        producer.send(record) { metadata, exception ->
            if (exception == null) {
                logger.info(
                    """
                   Received new metadata:
                   Key: $key
                   Topic: ${metadata.topic()}
                   Partition: ${metadata.partition()}
                   Offset: ${metadata.offset()}
                   Timestamp: ${metadata.timestamp()}
                """.trimIndent()
                )
            } else logger.error("error whilst producing: $exception")
        }

        producer.flush()
        producer.close()
    }

    private fun generateRecord(
        message: String,
        topic: String = "first_topic",
        key: String?
    ): ProducerRecord<String, String> =
        key?.let {
            ProducerRecord(topic, key, message)
        } ?: ProducerRecord(topic, message)
}

fun main() {
    var id = 0
    while (true) {
        Producer.sendMessage(
            message = "message id: $id",
            key = "id_$id"
        )
        Thread.sleep(1000)
        if (id < 10) {
            id++
        } else {
            id = 0
        }
    }
}