import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.common.errors.WakeupException
import org.apache.kafka.common.serialization.StringDeserializer
import org.slf4j.LoggerFactory
import java.time.Duration
import java.util.*
import java.util.concurrent.CountDownLatch
import kotlin.concurrent.thread


private val logger = LoggerFactory.getLogger(Consumer::class.java.name)

class Consumer {
    private val consumerProperties: Properties by lazy {
        val stringDeserializer = StringDeserializer::class.java.name
        val groupId = "Kotlin Consumer"
        return@lazy Properties().apply {
            setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, ConfigConstants.BOOTSTRAP_SERVERS)
            setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, stringDeserializer)
            setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, stringDeserializer)
            setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId)
            setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")
        }
    }
    private val delegate: KafkaConsumer<String, String> by lazy {
        return@lazy KafkaConsumer<String, String>(consumerProperties)
    }

    fun subscribe(
        topics: List<String> = listOf(ConfigConstants.DEFAULT_TOPIC),
        onConsumerClosed: (() -> Unit)? = null
    ) {
        try {
            while (true) {
                delegate.apply {
                    subscribe(
                        topics
                    )
                    poll(Duration.ofMillis(100)).forEach(::logConsumedRecord)
                }
            }
        } catch (exception: WakeupException) {
            logger.info("Received shutdown signal")
        } finally {
            logger.info("Closing consumer")
            delegate.close()
            onConsumerClosed?.invoke()
        }
    }

    private fun logConsumedRecord(record: ConsumerRecord<String, String>) {
        logger.info(
            """
                Key: ${record.key()}
                Partition: ${record.partition()}
                Topic: ${record.topic()}
                Offset: ${record.offset()}
                Timestamp: ${record.timestamp()}
                ThreadId: ${Thread.currentThread().id}
            """.trimIndent()
        )
    }

    fun shutdown() {
        logger.info("Request to shutdown consumer")
        delegate.wakeup()  //interrupts the consumer poll - Throws the WakeupException
    }
}

class ConsumerPoolManager(val numOfConsumers: Int) {

    fun subscribe() {
        logger.info("Creating consumer threads")

        val consumers = (0 until numOfConsumers).map { Consumer() }
        val latch = CountDownLatch(numOfConsumers)

        Runtime.getRuntime().addShutdownHook(thread(start = false) {
            consumers.forEach {
                it.shutdown()
            }
            latch.await()
        })

        consumers.forEach { consumer ->
            thread {
                consumer.subscribe {
                    logger.info("Subscription finished")
                }
            }
        }
    }
}

fun main() =
    ConsumerPoolManager(
        numOfConsumers = 2
    ).subscribe()
